# NDEF UID/Counter Mirring for the NTAG21x

Assuming a PC/SC NFC reader/writer.  I use a uTrust 3700, but an ACR122U should work as well.

1. Provide a URL with `{uid}` and/or `{counter}` where you'd like it mirrored: `node index.js "https://badgesof.ericbetts.dev/uid={uid}"`
2. It'll print some debug info and the newly written ndef: `  newNdef: [ 'https://badgesof.ericbetts.dev/uid=045F200A8F6580' ]`
3. NOTE: NTAG21x has specific limitations on the location of the uid and counter offsets.  I've attempted to check for these, but no guarantee.
